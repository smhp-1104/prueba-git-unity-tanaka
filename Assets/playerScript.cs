﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript : MonoBehaviour
{
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 velocidad = new Vector2(0, 0);

        if(Input.GetKey(KeyCode.RightArrow))
        {
            velocidad.x = 15;         
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            velocidad.x = -15;
        }
        rb.velocity = velocidad;

        
    }
}
